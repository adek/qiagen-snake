import {ChangeDetectionStrategy, Component, HostListener, OnInit} from '@angular/core';
import {SnakeFieldTypeEnum} from "./snake-field-type.enum";

interface Pos {
    posX: number;
    posY: number;
}

@Component({
    selector: 'app-snake',
    templateUrl: './snake.component.html',
    styleUrls: ['./snake.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SnakeComponent implements OnInit {
    items: SnakeFieldTypeEnum[][];

    @HostListener('document:keydown', ['$event']) keydown(event: KeyboardEvent): void {
        switch (event.key) {
            case 'ArrowLeft':
                this.goLeft();
                break;
            case 'ArrowDown':
                this.goDown();
                break;
            case 'ArrowRight':
                this.goRight();
                break;
            case 'ArrowUp':
                this.goUp();
                break;
        }
    }

    private get headItem(): Pos {
        return this.snakeItems[this.BODY_LENGTH];
    }

    private snakeItems: Pos[] = [];
    private readonly MAX = 7;
    private readonly BODY_LENGTH = 4;

    constructor() {
        this.items = new Array(7);

        for (let i = 0; i < this.MAX; i++) {
            this.items[i] = new Array(7);

            for (let j = 0; j < this.MAX; j++) {
                this.items[i][j] = SnakeFieldTypeEnum.NO_SNAKE;
            }
        }
    }

    ngOnInit(): void {
        this.initializeSnake();
    }

    private initializeSnake(): void {
        for (let i = 0; i < this.BODY_LENGTH; i++) {
            this.markGridItem(i, 0);
            this.snakeItems.push({posX: i, posY: 0})
        }

        this.markGridItem(this.BODY_LENGTH, 0, SnakeFieldTypeEnum.HEAD);
        this.snakeItems.push({posX: this.BODY_LENGTH, posY: 0});
    }

    private markGridItem(posX: number, posY: number, type = SnakeFieldTypeEnum.BODY): void {
        this.items[posY][posX] = type;
    }

    private goLeft(): void {
        if (this.headItem.posX === 0) {
            return;
        }

        this.moveTo(this.headItem.posX - 1, this.headItem.posY);
    }

    private goRight(): void {
        if (this.headItem.posX === this.MAX - 1) {
            return;
        }

        this.moveTo(this.headItem.posX + 1, this.headItem.posY);
    }

    private goUp(): void {
        if (this.headItem.posY === 0) {
            return;
        }

        this.moveTo(this.headItem.posX, this.headItem.posY - 1);
    }

    private goDown(): void {
        if (this.headItem.posY === this.MAX - 1) {
            return;
        }

        this.moveTo(this.headItem.posX, this.headItem.posY + 1);
    }

    private moveTo(posX: number, posY: number): void {
        // we don't allow snake to eat himself
        if (this.snakeItems.some(item => item.posX === posX && item.posY === posY)) {
            return;
        }

        this.markGridItem(this.headItem.posX, this.headItem.posY, SnakeFieldTypeEnum.BODY);
        this.markGridItem(posX, posY, SnakeFieldTypeEnum.HEAD);
        this.markGridItem(this.snakeItems[0].posX, this.snakeItems[0].posY, SnakeFieldTypeEnum.NO_SNAKE);

        this.snakeItems.push({
            posX, posY
        });
        this.snakeItems.shift();
    }
}
