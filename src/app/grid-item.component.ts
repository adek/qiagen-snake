import {Component, HostBinding, Input} from '@angular/core';
import {SnakeFieldTypeEnum} from "./snake-field-type.enum";

@Component({
    selector: 'app-grid-item',
    templateUrl: './grid-item.component.html',
    styleUrls: ['./grid-item.component.scss']
})
export class GridItemComponent {
    @Input() set type(value: SnakeFieldTypeEnum) {
        switch (value) {
            case SnakeFieldTypeEnum.BODY:
                this.class = 'body';
                break;
            case SnakeFieldTypeEnum.HEAD:
                this.class = 'head';
                break;
        }
    }

    @HostBinding('class') class = '';
}
