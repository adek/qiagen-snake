import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {GridItemComponent} from "./grid-item.component";
import {SnakeComponent} from "./snake.component";

@NgModule({
  declarations: [
    AppComponent,
    SnakeComponent,
    GridItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
