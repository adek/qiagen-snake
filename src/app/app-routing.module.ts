import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SnakeComponent} from "./snake.component";

const routes: Routes = [{ path: '', redirectTo: 'my-root', pathMatch: 'prefix' }, { path: 'my-root', component: SnakeComponent }];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
