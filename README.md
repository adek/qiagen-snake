# QiagenSnake

**Witam,**

Poza wyrzuceniem niepotrzebnych komponentów zrobiłem parę zmian w stosunku do aplikacji z naszej rozmowy:
1. zamiast zapisywać w kafelku obiekt 
   > { isHead: boolean, isBody: boolean }

   wprowadziłem enum
   > export enum SnakeFieldTypeEnum {
   NO_SNAKE,
   BODY,
   HEAD
   }
   
   z liczbą która określa jak powinien być oznaczony kafelek co po pierwsze zmniejszyło kod
   a po drugie z pewnością powinno podnieść performance :)


2. usunąłem Rxjs, zamiast tego zastosowałem
   > @HostListener('document:keydown', ['$event'])
   
   bo to bardziej "angularowe" :)

3. w komponencie GridItem stylowanie przeniosłem do samego komponentu i zastosowałem
   > @HostBinding('class')

4. dodałem tablicę
   > private snakeItems: {posX: number; posY: number; }[] = [];
   
   gdzie są przechowywane koordynaty elementów węża co pozwala na całkiem łatwe manipulowanie pozycją :)
